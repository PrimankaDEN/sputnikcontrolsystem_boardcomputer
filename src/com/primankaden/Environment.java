package com.primankaden;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Environment extends Component {
    private double mFlyWheel, mRocketEngine, Q, w, moment, wFlyWheel;

    public static void main(String[] args) throws IOException {
        new Environment().init();
    }

    public void init() throws IOException {
        Socket sFlyWheel = new Socket("localhost", 33105);
        DataOutputStream dosFlyWheel = new DataOutputStream(sFlyWheel.getOutputStream());
        DataInputStream disFlyWheel = new DataInputStream(sFlyWheel.getInputStream());

        Socket sRocket = new Socket("localhost", 33106);
        DataOutputStream dosRocket = new DataOutputStream(sRocket.getOutputStream());
        DataInputStream disRocket = new DataInputStream(sRocket.getInputStream());

        Socket sAngel = new Socket("localhost", 33108);
        DataOutputStream dosAngel = new DataOutputStream(sAngel.getOutputStream());
        DataInputStream disAngel = new DataInputStream(sAngel.getInputStream());

        Socket sVelocity = new Socket("localhost", 33107);
        DataOutputStream dosVelocity = new DataOutputStream(sVelocity.getOutputStream());
        DataInputStream disVelocity = new DataInputStream(sVelocity.getInputStream());

        long startTime = new Date().getTime();
        long currentTime = startTime;
        List<ChartFrame.Data> data = new ArrayList<>();

        while (currentTime < startTime + DURATION) {
            mFlyWheel = disFlyWheel.readDouble();
            wFlyWheel = disFlyWheel.readDouble();
            dosFlyWheel.writeDouble(TRASH);
            dosFlyWheel.flush();

            mRocketEngine = disRocket.readDouble();
            dosRocket.writeDouble(TRASH);
            dosRocket.flush();

            disVelocity.readDouble();
            dosVelocity.writeDouble(w);
            dosVelocity.flush();

            disAngel.readDouble();
            dosAngel.writeDouble(Q);
            dosAngel.flush();

            processData();
            currentTime = new Date().getTime();
            ChartFrame.Data d = new ChartFrame.Data();
            d.time = currentTime - startTime;
            d.momentWheel = -mFlyWheel;
            d.theoreticQ = mRocketEngine;
            d.Q = Q;
            d.Qs = w;
            d.wFlyWheel = wFlyWheel;
            data.add(d);
            try {
                Thread.currentThread().sleep(TICK_DURATION);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("m " + moment + " mW " + mFlyWheel + " mR " + mRocketEngine + " Q " + Q + " w " + w + " tD " + (currentTime - startTime));
        }

        ChartFrame frame = new ChartFrame("title1", "title2", data);
        frame.pack();
        frame.setVisible(true);
    }

    private long oldTime = new Date().getTime();

    private void processData() {
        moment = INNER_MOMENT - (mFlyWheel + mRocketEngine);
        double newTime = new Date().getTime(), deltaTime = (newTime - oldTime) / DELTA;
        this.w = this.w + deltaTime * moment / J_SPUTNIK;
        this.Q = this.Q + deltaTime * this.w;
    }
}
