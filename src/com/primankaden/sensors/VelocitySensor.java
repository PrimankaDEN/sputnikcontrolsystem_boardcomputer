package com.primankaden.sensors;

public class VelocitySensor extends AbsSensor {
    public static void main(String[] args) {
        AbsSensor sensor = new VelocitySensor();
        sensor.initServerConnection(33107, sensor::runServerSource);
        sensor.initServerConnection(33102, sensor::runServerDestination);
    }
}
