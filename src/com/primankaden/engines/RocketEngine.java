package com.primankaden.engines;

import com.primankaden.Component;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class RocketEngine extends Component {
    protected double rRequired;

    @Override
    protected void resetServerStats() {
        rRequired = 0;
    }

    public static void main(String[] args) {
        RocketEngine c = new RocketEngine();
        c.initServerConnection(33106, c::runServer);
        c.initServerConnection(33104, c::runClient);
    }

    protected void runClient(DataOutputStream dos, DataInputStream dis) throws IOException {
        dos.writeDouble(TRASH);
        dos.flush();
        rRequired = dis.readDouble();
        System.out.println(rRequired);
    }

    protected void runServer(DataOutputStream dos, DataInputStream dis) throws IOException {
        dos.writeDouble(computeMoment());
        dos.flush();
        dis.readDouble();
    }

    protected double computeMoment() {
        double rRequired = this.rRequired;
        if (rRequired > R_THRESHOLD) {
            return M_MAX;
        } else if (rRequired < -R_THRESHOLD) {
            return -M_MAX;
        } else return 0;
    }

}
