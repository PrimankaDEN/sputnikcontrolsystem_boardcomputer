package com.primankaden;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

public abstract class Component {
    public static final long TICK_DURATION = 1;
    protected static final double TRASH = 777777;
    protected static final double DELTA = 1000, DURATION = 5000;

    //computer
    // a0 == aQ, a1 == aW
    public static final double A_W = 20, A_Q = 1;
    public static final double J_SPUTNIK = 20, INNER_MOMENT = 0.02;

    //fly wheel
    public static final double W_MAX = 900;
    public static final double E_MAX = 10;
    public static final double I = 0.02;

    //rocket
    public static final double M_MAX = 0.3;
    public static final double R_THRESHOLD = 10;

    public interface Callback {
        void onConnect(DataOutputStream dos, DataInputStream dis) throws IOException;
    }

    public void initServerConnection(final int port, final Callback callback) {
        Thread serverThread = new Thread() {
            @Override
            public void run() {
                try {
                    ServerSocket ss = new ServerSocket(port);
                    for (; ; ) {
                        try {
                            Socket s = ss.accept();
                            resetServerStats();
                            System.out.println("Client connected " + port);
                            DataOutputStream dos = new DataOutputStream(s.getOutputStream());
                            DataInputStream dis = new DataInputStream(s.getInputStream());
                            for (; ; ) {
                                callback.onConnect(dos, dis);
                            }
                        } catch (IOException e) {
                            System.out.println(e.getLocalizedMessage() + " " + port);
                        }
                    }
                } catch (IOException e) {
                    System.out.println(e.getLocalizedMessage());
                }
            }
        };
        serverThread.start();
    }

    protected void resetServerStats(){};
}
