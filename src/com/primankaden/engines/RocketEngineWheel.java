package com.primankaden.engines;

import com.primankaden.Component;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class RocketEngineWheel extends Component {
    protected double rRequired;
    public static final double M_MAX = 0.08;
    public static final double R_THRESHOLD = 5;

    public static void main(String[] args) {
        RocketEngineWheel c = new RocketEngineWheel();
        c.initServerConnection(33106, c::runServer);
        c.initClientConnection("localhost", 33104, c::runClient);
    }

    protected void runClient(DataOutputStream dos, DataInputStream dis) throws IOException {
        rRequired = dis.readDouble();
        dos.writeDouble(1);
        dos.flush();
    }

    protected void runServer(DataOutputStream dos, DataInputStream dis) throws IOException {
        dos.writeDouble(computeMoment());
        dos.flush();
        dis.read(new byte[1]);
    }

    protected double computeMoment() {
        if (rRequired > R_THRESHOLD) {
            return M_MAX;
        } else if (rRequired < -R_THRESHOLD) {
            return -M_MAX;
        } else return 0;
    }

}
