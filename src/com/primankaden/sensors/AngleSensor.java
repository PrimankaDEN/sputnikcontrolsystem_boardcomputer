package com.primankaden.sensors;

public class AngleSensor extends AbsSensor {
    public static void main(String[] args) {
        AbsSensor sensor = new AngleSensor();

        sensor.initServerConnection(33108, sensor::runServerSource);
        sensor.initServerConnection(33101, sensor::runServerDestination);

    }
}
