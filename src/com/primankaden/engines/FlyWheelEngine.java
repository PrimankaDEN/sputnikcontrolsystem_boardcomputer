package com.primankaden.engines;

import com.primankaden.Component;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Date;

public class FlyWheelEngine extends Component {
    protected double eRequired;
    protected double wCurrent = 0;
    private boolean isActive = true;

    @Override
    protected void resetServerStats() {
        wCurrent=0;
        eRequired=0;
        isActive=true;
        prevTime = new Date().getTime();
    }

    public static void main(String[] args) {

        FlyWheelEngine c = new FlyWheelEngine();
        c.initServerConnection(33105, c::runServer);
        c.initServerConnection(33103, c::runClient);

    }

    protected void runClient(DataOutputStream dos, DataInputStream dis) throws IOException {
        dos.writeDouble(wCurrent);
        dos.writeBoolean(isActive);
        dos.flush();
        eRequired = dis.readDouble();
    }

    protected void runServer(DataOutputStream dos, DataInputStream dis) throws IOException {
        dos.writeDouble(computeMoment());
        dos.writeDouble(wCurrent);
        dos.flush();
        dis.readDouble();
    }

    private long prevTime = new Date().getTime();

    protected double computeMoment() {
        double eRequired = this.eRequired;

        if (Math.abs(eRequired) > E_MAX) {
            eRequired = eRequired > 0 ? E_MAX : -E_MAX;
        }
        if (Math.abs(wCurrent) > W_MAX * 0.99) {
            eRequired = 0;
            isActive = false;
        }

        long newTime = new Date().getTime();
        double timeDelta = (newTime - prevTime) / DELTA;
        wCurrent += eRequired * timeDelta;

        System.out.println(" wC " + wCurrent + " eR " + eRequired + " tD " + timeDelta);
        prevTime = newTime;
        return I * eRequired;
    }
}
