package com.primankaden;

import java.util.ArrayList;
import java.util.List;

public class MainThread {
    //computer
    // a0 == aQ, a1 == aW
    public static final double A_W = 10, A_Q = 2;
    public static final double J_SPUTNIK = 20, INNER_MOMENT = 0.02;

    //fly wheel
    public static final double W_MAX = 900;
    public static final double E_MAX = 10;
    public static final double E_STOP = 5;
    public static final double I = 0.02;

    //rocket
    public static final double M_MAX = 30;
    public static final double R_THRESHOLD = 1;

    public static final int COUNT = 100000;
    public static final double TICK = 0.1d;

    public static final int BREAK_ANGEL_TIME = 5000;

    double Q = 0, W = 0, wFlyWheel = 0;
    double eRequired;

    public static void main(String[] args) {
        new MainThread().compute();
    }

    boolean isFlyWheelActive = true;
    double flyMoment, rocketMoment;

    void compute() {
        List<ChartFrame.Data> data = new ArrayList<>();
        double prevQ = 0, prevW = 0;
        for (int i = 0; i < COUNT; i++) {
            double theoreticQ = prevQ + W * TICK;
            boolean isAngleCorrect = Integrator.integrate(theoreticQ, Q);
            if (!isAngleCorrect) {
                Q = theoreticQ;
            }

            double theoreticW = (Q - prevQ) / TICK;
            boolean isVelocityCorrect = Integrator.difference(theoreticW, W);
            if (!isVelocityCorrect) {
                W = theoreticW;
            }

            eRequired = A_Q * Q + A_W * W;

            flyMoment = computeFlyWheelMoment(isFlyWheelActive ? eRequired : -E_STOP);
            rocketMoment = computeRocketEngineMoment(isFlyWheelActive ? 0 : eRequired);

            double moment = INNER_MOMENT - (flyMoment + rocketMoment);

            prevQ = Q;
            prevW = W;
            this.W = this.W + TICK * moment / J_SPUTNIK;
            if (i > BREAK_ANGEL_TIME) {
                Q = 0;
            } else {
                this.Q = this.Q + TICK * this.W;
            }


            ChartFrame.Data d = new ChartFrame.Data();
            d.time = (long) (i);
            d.momentWheel = flyMoment;
            d.theoreticQ = theoreticQ;
            d.Q = Q;
            d.Qs = W;
            d.wFlyWheel = wFlyWheel / 500d;
            d.isQCorrect = isAngleCorrect;
            d.isWCorrect = isVelocityCorrect;
            data.add(d);
            if (wFlyWheel < -1) {
                break;
            }
        }

        ChartFrame frame = new ChartFrame("title1", "title2", data);
        frame.pack();
        frame.setVisible(true);
    }

    double computeFlyWheelMoment(double eRequired) {
        if (Math.abs(eRequired) > E_MAX) {
            eRequired = eRequired > 0 ? E_MAX : -E_MAX;
        }
        wFlyWheel += eRequired * TICK;

        if (Math.abs(wFlyWheel) > W_MAX) {
            wFlyWheel = wFlyWheel > 0 ? W_MAX : -W_MAX;
            isFlyWheelActive = false;
        }

        return I * eRequired;

    }

    double computeRocketEngineMoment(double eRequired) {
        if (eRequired > R_THRESHOLD) {
            return M_MAX;
        } else if (eRequired < -R_THRESHOLD) {
            return -M_MAX;
        } else return 0;
    }
}
