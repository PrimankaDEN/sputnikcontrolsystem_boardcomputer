package com.primankaden;

import com.primankaden.engines.FlyWheelEngine;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class BoardComputer extends Component {
    protected double Q = 0, w = 0, wFlyWheel;
    protected boolean isFlyWheelActive = true;
    protected boolean isFlyWheelStoped = false;
    protected double eRequired;

    public static void main(String[] args) throws IOException {
        final BoardComputer c = new BoardComputer();
        c.initServerConnection(33103, (dos, dis) -> {

        });

        c.init();
    }

    private void init() throws IOException {
        Socket sVelocity = new Socket("localhost", 33102);
        DataOutputStream dosVelocity = new DataOutputStream(sVelocity.getOutputStream());
        DataInputStream disVelocity = new DataInputStream(sVelocity.getInputStream());

        Socket sAngel = new Socket("localhost", 33101);
        DataOutputStream dosAngel = new DataOutputStream(sAngel.getOutputStream());
        DataInputStream disAngel = new DataInputStream(sAngel.getInputStream());

        Socket sFlyWheel = new Socket("localhost", 33103);
        DataOutputStream dosFlyWheel = new DataOutputStream(sFlyWheel.getOutputStream());
        DataInputStream disFlyWheel = new DataInputStream(sFlyWheel.getInputStream());

        Socket sRocket = new Socket("localhost", 33104);
        DataOutputStream dosRocket = new DataOutputStream(sRocket.getOutputStream());
        DataInputStream disRocket = new DataInputStream(sRocket.getInputStream());

        while (true) {
            w = disVelocity.readDouble();
            dosVelocity.writeDouble(TRASH);
            dosVelocity.flush();

            Q = disAngel.readDouble();
            dosAngel.writeDouble(TRASH);
            dosAngel.flush();

            eRequired = A_W * w + A_Q * Q;

            disRocket.readDouble();
            dosRocket.writeDouble(isFlyWheelActive ? 0 : eRequired);
            dosRocket.flush();

            wFlyWheel = disFlyWheel.readDouble();
            isFlyWheelActive = disFlyWheel.readBoolean();
            dosFlyWheel.writeDouble(isFlyWheelActive ? eRequired : isFlyWheelStoped ? 0 : -FlyWheelEngine.E_MAX);
            dosFlyWheel.flush();

//            if (Math.abs(wFlyWheel) >= FlyWheelEngine.W_MAX*0.95) {
//                isFlyWheelActive = false;
//            }
            if (!isFlyWheelActive && Math.abs(wFlyWheel) < 1) {
                isFlyWheelStoped = true;
            }
            System.out.println("eR " + eRequired + " " + isFlyWheelActive + " wF " + wFlyWheel);

            try {
                Thread.currentThread().sleep(TICK_DURATION);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
