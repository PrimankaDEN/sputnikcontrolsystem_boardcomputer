package com.primankaden.sensors;

import com.primankaden.Component;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public abstract class AbsSensor extends Component {
    private double value = 0;

    protected void runServerSource(DataOutputStream dos, DataInputStream dis) throws IOException {
        dos.writeDouble(TRASH);
        dos.flush();
        value = dis.readDouble();
    }

    protected void runServerDestination(DataOutputStream dos, DataInputStream dis) throws IOException {
        dos.writeDouble(value);
        dos.flush();
        dis.readDouble();
        System.out.println(this.getClass().getName() + "  " + value);
    }

    @Override
    protected void resetServerStats() {
        value = 0;
    }
}
