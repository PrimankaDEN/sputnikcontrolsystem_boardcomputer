package com.primankaden;

public class Integrator {
    public static final double DELTA = 0.3;


    public static boolean integrate(double theoreticQ, double currentQ) {
        return !(Math.abs(theoreticQ) > Math.abs(currentQ) * (1 + DELTA)
                || Math.abs(theoreticQ) < Math.abs(currentQ) * (1 - DELTA))
                && theoreticQ * currentQ >= 0;
    }

    public static boolean difference(double theoreticW, double currentW) {
        return !(Math.abs(theoreticW) > Math.abs(currentW) * (1 + DELTA)
                || Math.abs(theoreticW) < Math.abs(currentW) * (1 - DELTA))
                && theoreticW * currentW >= 0;
    }
}
