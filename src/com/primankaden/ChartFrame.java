package com.primankaden;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.util.List;

public class ChartFrame extends JFrame {
    private static final long serialVersionUID = 1L;

    public ChartFrame(String applicationTitle, String chartTitle, List<Data> list) {
        super(applicationTitle);
        setData(list);
        // based on the dataset we create the chart
        JFreeChart chart = createChart();
        // we put the chart into a panel
        ChartPanel chartPanel = new ChartPanel(chart);
        // default size
        chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
        // add it to our application
        setContentPane(chartPanel);

    }

    public static class Data {
        public long time;
        public double momentWheel;
        public double theoreticQ;
        public double Q;
        public double Qs;
        public double wFlyWheel;
        public boolean isQCorrect;
        public boolean isWCorrect;
    }

    private XYSeriesCollection set;

    public void setData(List<Data> list) {
        XYSeries moment = new XYSeries("M wheel");
        XYSeries theoreticQ = new XYSeries("Theoretic Q");
        XYSeries Q = new XYSeries("Q");
        XYSeries Qs = new XYSeries("W");
        XYSeries wF = new XYSeries("W wheel");
        XYSeries Qc = new XYSeries("Q correct");
        XYSeries Wc = new XYSeries("W correct");
        for (Data d : list) {
            moment.add(d.time, d.momentWheel);
            theoreticQ.add(d.time, d.theoreticQ);
            Q.add(d.time, d.Q);
            Qs.add(d.time, d.Qs);
            wF.add(d.time, d.wFlyWheel);
            Qc.add(d.time, d.isQCorrect ? 0 : -1);
            Wc.add(d.time, d.isWCorrect ? 0 : 1);
        }
        set = new XYSeriesCollection();
        set.addSeries(moment);
        set.addSeries(Q);
        set.addSeries(Qs);
        set.addSeries(theoreticQ);
        set.addSeries(wF);
        set.addSeries(Qc);
        set.addSeries(Wc);
    }


    /**
     * Creates a chart
     */

    private JFreeChart createChart() {

        JFreeChart chart = ChartFactory.createXYLineChart("XY Chart", // Title
                "x-axis", // x-axis Label
                "y-axis", // y-axis Label
                set, // Dataset
                PlotOrientation.VERTICAL, // Plot Orientation
                true, // Show Legend
                true, // Use tooltips
                false);
//
//        PiePlot3D plot = (PiePlot3D) chart.getPlot();
//        plot.setStartAngle(290);
//        plot.setDirection(Rotation.CLOCKWISE);
//        plot.setForegroundAlpha(0.5f);
        return chart;

    }
}
